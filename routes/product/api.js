const router = require('express').Router();
const Product = require('../../models/product');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
// const express = require('express');
// const secureRoutes = express.Router();

const apiOperation = (req, res, crudMethod, optionsObj)=> {
	const bodyParams = Methods.initializer(req, Product);
	console.log('bodyParams: ', bodyParams);
	crudMethod(bodyParams, optionsObj, (err, product) => {
		console.log('\nproduct details', product);
		res.send(product);
	})
}

router.get('/', (req, res) => {
	res.send('This is the api route for product management');
});

router.get('/getallproducts', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = req.query.table;
					Product.selectTable(table);
					Product.query('Product',(err,products)=>{
						res.send(products);
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/getproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Product.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}	
});

router.post('/addproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
		if(token){
			console.log(token);
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					console.log(decode.storeName);
					const bodyParams = Methods.initializer(req, Product);
					Product.createItem(bodyParams, {
						table: decode.storeName+"_store_data",
						overwrite: false
					}, (err, product) => {
						console.log('\nproduct details', product);
						res.send(product);
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.put('/updateproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Product.updateItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.delete('/deleteproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				apiOperation(req, res, Product.deleteItem);
			}
        })
    }
    else{
        res.send("please send a token");
    }
});

module.exports = router;